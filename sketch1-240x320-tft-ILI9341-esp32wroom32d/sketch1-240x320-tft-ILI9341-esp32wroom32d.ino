#include <TJpg_Decoder.h> //IMAGE DECODER LIBRARY
#include <SPIFFS.h> //SPIFF LIBRARY FOR ESP32 ARDUINO CORE
#include <Arduino_GFX_Library.h> // ARDUINO GFX LIBRARY (NEEDED FOR THE DISPLAY)

// CONNECTIONS

#define TFT_SCK    18
#define TFT_MOSI   23
#define TFT_MISO   19
#define TFT_CS     22
#define TFT_DC     21
#define TFT_RESET  17

Arduino_ESP32SPI *bus = new Arduino_ESP32SPI(TFT_DC, TFT_CS, TFT_SCK, TFT_MOSI, TFT_MISO);
Arduino_ILI9341 *display = new Arduino_ILI9341(bus, TFT_RESET);

//DECODE FUNCTION
bool onDecode(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t* bitmap)
{
  display->draw16bitRGBBitmap(x, y, bitmap, w, h);
 
  return 1;
}

//FLASHING SCREEN FUNCTION
void flashingScreen(){
  display->fillScreen(BLACK);
  display->fillScreen(WHITE);
  display->fillScreen(0xFCA0); //Bhagwa Rang
  for(int i=0;i<12;i++){
    digitalWrite(2, LOW);
    delay(25*i);
    digitalWrite(2, HIGH);
    delay(25*i);
  }
}

void setup() {
  Serial.begin(115200);
  pinMode(2, OUTPUT);
  digitalWrite(2, HIGH);
  if(!SPIFFS.begin()) {
    Serial.println("SPIFFS init failed");
    return;
  }
  Serial.println("SPIFFS init finished");
 
  display->begin();
  flashingScreen();
  digitalWrite(2, HIGH);
  TJpgDec.setCallback(onDecode);
  TJpgDec.drawFsJpg(0, 0, "/mandir1.jpg");
  delay(2000);
  TJpgDec.drawFsJpg(0, 0, "/mandir2.jpg");
  delay(2500);
  TJpgDec.drawFsJpg(0, 0, "/RamLalla.jpg");
  
   
}
 
void loop() {}
